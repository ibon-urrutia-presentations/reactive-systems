= Reactive Systems
Ibon Urrutia <urrutia.ibon@gmail.com>
:icons: font
:toc:
:revealjs: https://revealjs.com/#/[reveal.js]
:asciidoc-revealjs: https://github.com/asciidoctor/asciidoctor-reveal.js/[Asciidoctor reveal.js converter]
:asciidoc: https://asciidoctor.org[Asciidoctor]
:asciidoctor-diagrams: https://asciidoctor.org/docs/asciidoctor-diagram/[Asciidoctor diagram]
:reactive-systems: https://www.oreilly.com/radar/reactive-programming-vs-reactive-systems/[Reactive Systems]

A {revealjs} presentation about {reactive-systems}.

== Prerequisites
You need to install `graphviz` (available in the package repositories of all
main linux distributions) in order to generate the asciidoc diagrams.

== Usage
- Clone this repository
+
[code,bash]
----
$ git clone https://gitlab.com/ibon-urrutia-presentations/reactive-systems
----
- Build with
+
[code,bash]
----
$ cd reactive-systems
$ gradle asciidoc
----
- Open `build/asciidoc/revealjs/index.html` in a browser

== Distribution
For creating a zip file distribution build it with:

[code,bash]
----
$ gradle presentation
----
