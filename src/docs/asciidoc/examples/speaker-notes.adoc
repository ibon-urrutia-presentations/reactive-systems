== Speaker notes

Press 'S' to see the speaker notes window

[.notes]
****
* this is the new block type for speaker notes: sidebar
* `notes` role required (written `.notes` in block attribute)
* source is slightly more compact
* and not tied to an admonition which can be useful in slides
****

=== Other Style

Other speaker notes styles

[.notes]
--
* Using open blocks work too
* Yay!
--

=== Old style

Hello Old Style Speaker Notes

[NOTE.speaker]
--
* notes using admonition block still work
* `aside` or `notes` roles also valid in addition to `speaker`
--
